import React from 'react';
import { Outlet } from 'react-router-dom';
import styled from 'styled-components';
import { ReactComponent as Logo } from '../assets/images/Logo.svg';
import SideBar from '../components/SideBar';
import HeaderDashboard from '../components/HeaderDashboard';

const Main = styled.div`
  width: 100%;
  min-height: 100vh;
  background: #F6F6F9;
  display: flex;
  overflow: hidden;
`;

const LeftSection = styled.div`
  width: 224px;
  min-height: 100vh;
  background: #FFFFFF;
  border-right: 1px #EAEAEF solid;
`;

const LogoHeader = styled.div`
  display: flex;
  justify-content: left;
  align-items: center;
  padding: 21px 12px;
  border-bottom: 1px #EAEAEF solid;

  & svg {
    width: 138px;
    height: 30px;
  }
`;


const RightSection = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 224px);
  margin-left: auto;
`;

const DashboardLayout = () => {
   return (
      <Main>
         <LeftSection>
            <LogoHeader>
               <Logo />
            </LogoHeader>
            <SideBar />
         </LeftSection>
         <RightSection>
            <HeaderDashboard />
            <Outlet />

         </RightSection>

      </Main>
   );
};

export default DashboardLayout;