import React, {Suspense} from 'react';
import ReactDOM from 'react-dom/client';
import {HashLoader} from 'react-spinners';


import App from './App';
import './i18Next';
import {BrowserRouter} from "react-router-dom";


export const Loader = () => {
    return (
        <div>
            <div
                style={{
                    width: '100%',
                    minHeight: '100vh',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <HashLoader color="#5FB5F3" size={70} speedMultiplier={2} />
            </div>
        </div>
    );
};

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <BrowserRouter>
        <Suspense fallback={<Loader />}>
            <App />
        </Suspense>
    </BrowserRouter>
);
