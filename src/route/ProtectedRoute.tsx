import { FC } from 'react';
import { Navigate } from 'react-router-dom';


export type ProtectedRouteProps = {
   authorized: boolean;
   component: FC
}

const ProtectedRoute: FC<ProtectedRouteProps> = ({ component: RouteComponent, authorized }) => {
   if (!authorized) return <Navigate to='/login' />;
   return <RouteComponent />;
};

export default ProtectedRoute;