import React, { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import DashboardLayout from '../layout/DashboardLayout';
import Main from '../pages/Main';

const Login = lazy(() => import('../pages/Login'));

const Index = () => {
   return (
      <>
         <Routes>
            <Route path='/login' element={<Login />} />
            <Route path='/dashboard' element={<ProtectedRoute authorized={true} component={DashboardLayout} />}>
               <Route index element={<Main />} />
               <Route path='teachers' element={<Main />} />
            </Route>
            <Route path='*' element={<Login />} />
         </Routes>
      </>
   );
};

export default Index;