import React from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { IoNotificationsSharp } from 'react-icons/io5';

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 72px;
  border-bottom: 1px #EAEAEF solid;
  background: #FFFFFF;
  padding: 0 40px 0 24px;
`;

const PathName = styled.div`
  & p {
    margin: 0;
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 32px;
    color: #32324D;
  }
`;

const Section = styled.div`
   display: flex;
  justify-content: space-between;
  align-items: center;
  
`;

const HeaderDashboard = () => {
   const location = useLocation();
   const pathname = location.pathname.replaceAll('/', '') === 'dashboard' ? 'Dashboard' : 'O\'qituvchilar';
   return (
      <Header>
         <PathName>
            <p>{pathname}</p>
         </PathName>
         <Section>
            sdasasd
         </Section>
      </Header>
   );
};

export default HeaderDashboard;