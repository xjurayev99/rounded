import React from 'react';
import { FaHome } from 'react-icons/fa';
import { BsPersonFill } from 'react-icons/bs';
import styled, { css } from 'styled-components';
import { NavLink } from 'react-router-dom';


const SideBarTag = styled.div`
  display: flex;
  flex-direction: column;
  padding: 24px 12px;

`;

const SideBarTitle = styled.h4`
  padding: 4px 12px;
  margin: 0 0 8px 0;
  font-style: normal;
  font-weight: 600;
  font-size: 11px;
  line-height: 16px;
  display: flex;
  align-items: center;
  text-transform: uppercase;
  color: #666687;

`;

const StyledNavLink = styled(NavLink)<{ active: boolean }>`
  display: flex;
  justify-content: left;
  align-items: center;
  padding: 8px 12px;
  text-decoration: none;
  border-radius: 4px;
  transition: all .2s ease;
  cursor: pointer;
  margin: 0 0 8px 0;

  & svg {
    transition: all .2s ease;
    width: 20px;
    height: 20px;
    margin-right: 14px;
    color: #666687;
  }

  & p {
    transition: all .2s ease;
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    display: flex;
    align-items: center;
    color: #666687;
    margin: 0;
  }

  &:hover {
    background: rgba(0, 134, 255, 0.1);
    transition: all .2s ease;

    & p {
      color: #0086FF;
    }

    & svg {
      color: #0086ff;
      fill: #0086ff;

    }

   
`;

const SideBar = () => {
   return (
      <>
         <SideBarTag>
            <SideBarTitle>
               asosiy
            </SideBarTitle>
            <StyledNavLink active={true} to='/dashboard/'>
               <FaHome />
               <p>Dashboard</p>
            </StyledNavLink>
            <StyledNavLink active to='/dashboard/teachers'>
               <BsPersonFill />
               <p>O’qituvchi</p>
            </StyledNavLink>
         </SideBarTag>
      </>
   );
};

export default SideBar;