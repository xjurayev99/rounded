import React, { useState } from 'react';
import styled from 'styled-components';
import { ReactComponent as Logo } from '../assets/images/Logo.svg';
import { ReactComponent as Eye } from '../assets/images/Eye.svg';


const Main = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #F6F6F9;
`;

const LoginCard = styled.div`
  display: flex;
  flex-direction: column;
  padding: 56px;
  background: #FFFFFF;
  border: 0.5px solid #EAEAEF;
  box-shadow: 0px 1px 4px rgba(33, 33, 52, 0.1);
  border-radius: 4px;
`;

const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const Title = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 32px;
  line-height: 40px;
  display: flex;
  align-items: center;
  text-align: center;
  color: #32324D;
  padding: 24px 0 6px;
  margin: 0;
  order: 1;
`;

const SubTitle = styled.h4`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  display: flex;
  align-items: center;
  text-align: center;
  color: #A5A5BA;
  order: 1;
  margin: 0;
  padding: 0;
`;

const Form = styled.form`
  min-width: 440px;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  margin: 32px 0 0;
`;
const FormItem = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  position: relative;
  margin: 0 0 24px;
`;
const Label = styled.label`
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  color: #32324D;
  margin-bottom: 4px;
`;

const Input = styled.input`
  width: calc(100% - 32px);
  background: #FFFFFF;
  border: 1px solid #DCDCE4;
  border-radius: 4px;
  flex: none;
  order: 1;
  align-self: stretch;
  flex-grow: 0;
  padding: 10px 16px;
`;

const ShowPass = styled.div`
  position: absolute;
  top: 32px;
  right: 14px;
  cursor: pointer;
`;

const CheckItem = styled.div`
  display: flex;
  margin: 0 0 24px;

  input {
    width: 20px;
    height: 20px;
    background: #FFFFFF;
    border: 1px solid #C0C0CF;
    border-radius: 4px;
    margin: 0 8px 0 0;

  }
`;

const CheckText = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  color: #32324D;
  margin: 0;


`;

const Btn = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px 16px;
  gap: 10px;
  width: 440px;
  height: 40px;
  background: #0086FF;
  border-radius: 4px;
  flex: none;
  order: 0;
  flex-grow: 1;
  border: none;
  outline: none;
  cursor: pointer;
  color: #FFFFFF;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
`;


const Login = () => {
   const [passType, setPassType] = useState<boolean>(true);
   return (
      <Main>
         <LoginCard>
            <Header>
               <Logo />
               <Title>
                  Xush kelibsiz!
               </Title>
               <SubTitle>
                  Login va Parolingizni kiriting
               </SubTitle>
            </Header>
            <Form  >
               <FormItem>
                  <Label>
                     Login
                  </Label>
                  <Input type='text' placeholder='Please enter your login' />
               </FormItem>
               <FormItem>
                  <Label>
                     Password
                  </Label>
                  <Input type={passType ? 'password' : 'text'} placeholder='Please enter your login' />
                  <ShowPass onClick={() => setPassType(!passType)}>
                     <Eye />
                  </ShowPass>
               </FormItem>
               <CheckItem>
                  <input type='checkbox' />
                  <CheckText>
                     Remember me
                  </CheckText>
               </CheckItem>
               <Btn type='submit'>
                  Login
               </Btn>
            </Form>
         </LoginCard>
      </Main>
   );
};

export default Login;